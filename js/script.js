// --------- Our Services ---------

tabsListElements = tabsList.querySelectorAll(".services__tabs__title");
tabsContentListElements = tabsContentList.querySelectorAll("li");

for (element of tabsListElements) {

    element.addEventListener("click", function() {
        for (const element of tabsListElements) {
            element.classList.remove("services__active__tab");
        }
        this.classList.toggle("services__active__tab");
        showContent(this.dataset.title);
    });
}

function showContent(forContent) {
    for (const element of tabsContentListElements) {
        element.style.display = "none";
        if (element.dataset.for === forContent) {
            element.style.display = "flex";
        }
    };
};

tabsListElements[0].classList.add("services__active__tab"); 
showContent(tabsListElements[0].dataset.title);

// --------- Our Amazing Work ---------

const gallery = {
    container: document.getElementById("workGallery"),
    addImagesBtn: document.getElementById("addImgs"),
    allImagesTab: document.getElementById("allImages"),
    filteresImagesTabs: document.querySelectorAll(".our_work__tabs__filtered"),
    section: document.getElementById("ourWorkSection"),
    loadedRows: 0,
    iterations: 0,
    maxIterations: 3,
    loadedImages: 0,

    imageObjects: [
            { url: "img/graphic design/graphic-design1.jpg", title: "Creative Design", category: "Graphic Design"},
            { url: "img/web design/web-design1.jpg", title: "Crafting Digital Experiences", category: "Web Design"},
            { url: "img/graphic design/graphic-design2.jpg", title: "Unleashing Creativity", category: "Graphic Design"},
            { url: "img/landing page/landing-page1.jpg", title: "Conversion Catalysts", category: "Landing Page"},
            { url: "img/web design/web-design2.jpg", title: "Pixel Perfect", category: "Web Design"},
            { url: "img/web design/web-design3.jpg", title: "Beyond Aesthetics", category: "Web Design"},
            { url: "img/wordpress/wordpress1.jpg", title: "Wordpress Powerhouse", category: "Wordpress"},
            { url: "img/graphic design/graphic-design3.jpg", title: "Inspiring Creativity", category: "Graphic Design"},
            { url: "img/wordpress/wordpress2.jpg", title: "Mastering Wordpress", category: "Wordpress"},
            { url: "img/landing page/landing-page2.jpg", title: "Engaging Experiences", category: "Landing Page"},
            { url: "img/graphic design/graphic-design4.jpg", title: "Where Creativity Flourishes", category: "Graphic Design"},
            { url: "img/landing page/landing-page3.jpg", title: "Irresistible Designs", category: "Landing Page"},
            { url: "img/graphic design/graphic-design5.jpg", title: "Where Creativity Flourishes", category: "Graphic Design"},
            { url: "img/wordpress/wordpress3.jpg", title: "Wordpress Wonders", category: "Wordpress"},
            { url: "img/web design/web-design4.jpg", title: "Web Design Innovations", category: "Web Design"},
            { url: "img/graphic design/graphic-design6.jpg", title: "Unconventional Design", category: "Graphic Design"},
            { url: "img/wordpress/wordpress4.jpg", title: "Wordpress Magic", category: "Wordpress"},
            { url: "img/graphic design/graphic-design7.jpg", title: "A Symphony of Creativity", category: "Graphic Design"},
            { url: "img/wordpress/wordpress5.jpg", title: "Wordpress Essentials", category: "Wordpress"},
            { url: "img/web design/web-design5.jpg", title: "Responsive Web Design", category: "Web Design"},
            { url: "img/landing page/landing-page4.jpg", title: "Click Conversions", category: "Landing Page"},
            { url: "img/graphic design/graphic-design8.jpg", title: "Embracing Creative Thinking", category: "Graphic Design"},
            { url: "img/wordpress/wordpress6.jpg", title: "Wordpress Unleashed", category: "Wordpress"},
            { url: "img/landing page/landing-page5.jpg", title: "Captivating Concepts", category: "Landing Page"},
            { url: "img/graphic design/graphic-design9.jpg", title: "Imaginative Design", category: "Graphic Design"},
            { url: "img/wordpress/wordpress7.jpg", title: "Wordpress Revolution", category: "Wordpress"},
            { url: "img/graphic design/graphic-design10.jpg", title: "Empowering Creativity", category: "Graphic Design"},
            { url: "img/landing page/landing-page6.jpg", title: "Conversion Wizards", category: "Landing Page"},
            { url: "img/web design/web-design6.jpg", title: "Cutting-Edge Web Design", category: "Web Design"},
            { url: "img/wordpress/wordpress8.jpg", title: "Wordpress Brilliance", category: "Wordpress"},
            { url: "img/graphic design/graphic-design11.jpg", title: "Crafting Creativity", category: "Graphic Design"},
            { url: "img/landing page/landing-page7.jpg", title: "Seamless User Journeys", category: "Landing Page"},
            { url: "img/wordpress/wordpress9.jpg", title: "Wordpress Simplified", category: "Wordpress"},
            { url: "img/graphic design/graphic-design12.jpg", title: "Designing Dreams", category: "Graphic Design"},
            { url: "img/wordpress/wordpress10.jpg", title: "Wordpress Domination", category: "Wordpress"},
            { url: "img/web design/web-design7.jpg", title: "Seamless User Journeys", category: "Web Design"},],

    createImage(imgObj) {
        const li = document.createElement("li");
        li.classList.add("our_work__gallery__item");
        li.innerHTML = `
        <img class="our_work__gallery__img" src="${imgObj.url}" alt="${imgObj.title}">
        <div class='our_work__gallery__caption'>
        <div class='our_work__gallery__hrefs_container'>
            <a href="#" class="gallery__caption__round gallery__caption__href">
                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759143 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922616 9.8266 0.0883684 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
                        fill="#1FDAB5" />
                </svg>
            </a>
            <a href="#" class="gallery__caption__round gallery__caption__square">
                <svg width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect width="12" height="11" fill="white" />
                </svg>
            </a>
        </div>
        <h4 class="gallery__caption__title">${imgObj.title}</h4>
        <p class="gallery__caption__category">${imgObj.category}</p>
    </div>`;
        this.container.append(li);
    },
    addAllImages() {
        if (this.iterations >= this.maxIterations) {
            return;
        } else {
            if (this.iterations > 0) {
                let height = this.section.clientHeight;
                let newHeight = height + 618 + "px";
                this.section.style.height = newHeight;
            }
            const ImgAmount = 12;
            for (let i = 0; i < ImgAmount; i++) {
                this.createImage(this.imageObjects[i + (ImgAmount * this.iterations)]);
            }
            this.iterations++;
            if (this.iterations == 3) {
                this.addImagesBtn.style.display = "none";
            }
        }
    },
    showImagesOfCategory(category) {
        this.container.innerHTML = "";
        this.section.style.height = "1212px";
        this.addImagesBtn.style.display = "none";

        let imagesOfCategory = this.imageObjects.filter(obj => obj.category == category);
        imagesOfCategory.forEach(img => this.createImage(img));
    },
    removeAllImagesContent() {
        this.iterations = 0;
        this.container.innerHTML = "";
        this.section.style.height = "1212px";
        this.addImagesBtn.style.display = "block";
    },
    changeActiveTab(toActive) {
        document.querySelectorAll(".our_work__tabs__title").forEach(function(tab) {
            tab.classList.remove("our_work__active__tab");
          });
          toActive.classList.add("our_work__active__tab");
    },
};

// For Button
  gallery.addImagesBtn.addEventListener("click", function() {
    gallery.addImagesBtn.innerHTML = `<div class="gallery__loading-animation"></div>`;
    setTimeout(function() {
        gallery.addImagesBtn.innerHTML = `
        <img src="img/icons/plus.svg" alt="Plus Icon">
        <span>Load More</span>`;
        gallery.addAllImages();
      }, 1000) }); 

// For Tabs "All"
gallery.allImagesTab.addEventListener("click", function() {
    if (gallery.allImagesTab.classList.contains("our_work__active__tab")) {
        return;
    } else {
        gallery.changeActiveTab(gallery.allImagesTab);
        gallery.removeAllImagesContent();
        gallery.addAllImages();
    }
});

// For Filtered Tabs
for (const tab of gallery.filteresImagesTabs) {
    tab.addEventListener("click", function() {
        gallery.changeActiveTab(tab);
        gallery.showImagesOfCategory(tab.dataset.title);
    } ); 
};

gallery.addAllImages();

// What People Say About theHam

const carousel = {
    nameField: document.getElementById("peopleName"),
    postField: document.getElementById("peoplePost"),
    imageField: document.getElementById("peopleImg"),
    quoteField: document.getElementById("peopleQuote"),
    arrowLeft: document.getElementById("arrowLeft"),
    arrowRight: document.getElementById("arrowRigth"),
    carouselContainer: document.getElementById("carouselContainer"),
    carouselImages: document.querySelectorAll(".people_say__carousel__img"),
    activeIndex: null,

    carouselCards: [
        {
            name: "Hasan Ali",
            post: "UX Designer",
            imageSrc: "img/What People Say/first.png",
            quote: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
        },
        {
            name: "Emma Lawson",
            post: "Software Engineer",
            imageSrc: "img/What People Say/second.png",
            quote: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at gravida augue. Vestibulum auctor, mauris non elementum mollis, urna neque cursus nibh, non elementum metus nisl eget ipsum. Sed eleifend ante quis metus facilisis, in gravida lorem posuere.",
        },
        {
            name: "Henry Anderson",
            post: "Data Scientist",
            imageSrc: "img/What People Say/third.png",
            quote: "Aliquam erat volutpat. Curabitur eget lacus malesuada, dapibus sem vitae, vestibulum ex. Nullam et nisi aliquet, bibendum sapien vel, consequat ligula. Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.",
        },
        {
            name: "Lily Carter",
            post: "Cybersecurity Analyst",
            imageSrc: "img/What People Say/fourth.png",
            quote: "Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis, in gravida lorem posuere, consequat ligula, quam dui laoreet sem, non dictum odio nisi quis massa.",
        },
    ],

    createCarousel() {
        for (let i = 0; i < this.carouselImages.length; i++) {
            this.carouselImages[i].setAttribute("src", this.carouselCards[i].imageSrc);
            if (this.carouselImages[i].classList.contains("carousel__active")) {
                this.createMainCard(this.carouselCards[i], i);
                this.carouselImages[i].style.transform = "translateY(-15px)";
            }
        }
    },
    createMainCard(card, index) {
        this.activeIndex = index;
        this.nameField.innerHTML = card.name;
        this.postField.innerHTML = card.post;
        this.quoteField.innerHTML = card.quote;
        this.imageField.setAttribute("src", card.imageSrc);
    },
    changeActiveCard(newCardIndex) {
        let newActiveCard = this.carouselImages[newCardIndex];
        this.carouselImages[this.activeIndex].style.transform = "translateY(0)";
        newActiveCard.style.transform = "translateY(-15px)";
        this.carouselImages.forEach(img => img.classList.remove("carousel__active"));
        newActiveCard.classList.add("carousel__active");
        this.createMainCard(this.carouselCards[newCardIndex], newCardIndex);
    },
    scrollRight() {
        if (this.activeIndex == (this.carouselImages.length - 1)) {
            this.changeActiveCard(0);
        } else {
            this.changeActiveCard(this.activeIndex + 1);
        }
    },
    scrollLeft() {
        if (this.activeIndex == 0) {
            this.changeActiveCard(this.carouselImages.length - 1);
        } else {
            this.changeActiveCard(this.activeIndex - 1);
        }
    },

};

carousel.createCarousel();

carousel.carouselContainer.addEventListener("click", function(event) {
    if (event.target.classList.contains("people_say__carousel__img")) {
        const imagesInContainer = Array.from(carousel.carouselContainer.querySelectorAll('img'));
        const index = imagesInContainer.indexOf(event.target);
        carousel.changeActiveCard(index);
    }
});

carousel.arrowRight.addEventListener("click", carousel.scrollRight.bind(carousel));
carousel.arrowLeft.addEventListener("click", carousel.scrollLeft.bind(carousel));
